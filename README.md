# Postgis

This postgis build based on official postgresql image.

Source code available in tags of projects https://gitlab.com/egeneralov/postgis/.

Docker HUB: https://hub.docker.com/r/egeneralov/postgis/

# Quickstart

First - run container:

```bash
docker run -d --name postgis \
-e POSTGRES_USER=egeneralov \
-e POSTGRES_PASSWORD=passwd \
-e POSTGRES_DB=pet_project \
-e PGDATA=/pgdata \
-v ${PWD}/pgdata:/pgdata \
-p 127.0.0.1:5432:5432 \
egeneralov/postgis:3.0.0
```

Connect to database and execute query:


```sql
-- Enable PostGIS
CREATE EXTENSION postgis;
-- Enable Topology
CREATE EXTENSION postgis_topology;
-- fuzzy matching needed for Tiger
CREATE EXTENSION fuzzystrmatch;
-- rule based standardizer
CREATE EXTENSION address_standardizer;
-- Enable US Tiger Geocoder
CREATE EXTENSION postgis_tiger_geocoder;
-- Enable PostGIS Advanced 3D
-- and other geoprocessing algorithms
-- sfcgal available in this image
CREATE EXTENSION postgis_sfcgal;
```

# Tags

## egeneralov/postgis:3.0.0
Postgis 3.0.0dev over postgres 10

## egeneralov/postgis:2.5.1
Postgis 2.5.1 over postgres 9.6

